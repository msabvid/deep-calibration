import torch
import torch.nn as nn
import numpy as np
import os
import math




class Path_Heston():
    
    """
    Right now this is only for one Maturity and one Strike
    Next step: extend it so that it creates the payoff for a surface of maturities x strikes
    """
    
    def __init__(self, timegrid, device="cpu"):
        #super(Path_Heston, self).__init__()
        self.timegrid = torch.tensor(timegrid).to(device)
        self.device=device

    def forward(self, S0, V0, sigma, mu, rho, K, MC_samples):
        batch_size = S0.shape[0]
        S_old = torch.repeat_interleave(S0, MC_samples, dim=0)
        V_old = torch.repeat_interleave(V0, MC_samples, dim=0)
        
        sigma_extended = torch.repeat_interleave(sigma, MC_samples, dim=0)
        mu_extended = torch.repeat_interleave(mu, MC_samples, dim=0)
        rho_extended = torch.repeat_interleave(rho, MC_samples, dim=0)
        
        K_extended = torch.repeat_interleave(K, MC_samples, dim=0)
        
        for i in range(1, len(self.timegrid)):
            h = self.timegrid[i] - self.timegrid[i-1]
            
            # correlated brownian motion
            dW1 = torch.sqrt(h) * torch.randn(MC_samples, 1, device=self.device)
            dW1 = dW1.repeat(batch_size, 1)
            
            dZ = torch.sqrt(h) * torch.randn(MC_samples, 1, device=self.device)
            dZ = dZ.repeat(batch_size, 1)
            
            dW2 = rho_extended*dW1 + torch.sqrt(1-rho_extended**2)*dZ
            
            # euler scheme
            S_new = S_old + sigma_extended * S_old * torch.sqrt(V_old) * dW1
            V_new = V_old + (mu_extended - V_old) * h + torch.sqrt(V_old) * dW2
            
            # we are done. Prepare for next iteration
            S_old, V_old = S_new, V_new
        
        # we calculate average of (S_T-K)_{+}
        zeros = torch.zeros_like(S_old)
        price = torch.cat([S_old - K_extended, zeros],1)
        price = torch.max(price, 1, keepdim=True)
        price_split = torch.split(price, MC_samples)
        avg_price = torch.cat([p.mean().view(1,1) for p in price_split], 0)
        
        return avg_price
                            


        
        