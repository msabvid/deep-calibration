import torch
import torch.nn as nn
import numpy as np
import argparse
from models import Net_SDE
from path_generators import Path_Heston




def train(model_ub, model_lb, optimizer, path_gen, args):
    
    for it in range(args.n_epochs):
        optimizer.zero_grad()
        
        # we sample. S0 and V0 are for the moment always constant
        S0 = torch.ones(args.batch_size, 1, device=device)
        V0 = torch.ones(args.batch_size, 1, device=device) * 0.3
        K = S0
        
        sigma = 0.01 + 0.5 * torch.rand(args.batch_size, 1, device=device)
        mu = 0.01 + 0.5 * torch.rand(args.batch_size, 1, device=device)
        rho = torch.rand(args.batch_size, 1, device=device)
        
        avg_price = path_gen.forward(S0, V0, sigma, mu, rho, K, args.MC_samples)
        pred_ub = model_ub(dim, S0, sigma, mu, rho, K, MC_samples)
        pred_lb = model_lb(dim, S0, sigma, mu, rho, K, MC_samples)
        
        # TODO: build cost function for upper bound
        
        # TODO: build cost function for lower bound
        
        
    
    
    
    



if __name__=='__main__':
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--device', action="store", type=int, default=0)
    parser.add_argument('--epochs', action="store", type=int, default=100)
    parser.add_argument('--N_steps', action="store", type=int, default=100)
    parser.add_argument('--dim', action="store", type=int, default=2)
    parser.add_argument('--batch_size', action="store", type=int, default=5000)
    parser.add_argument('--MC_samples', action="store", type=int, default=5000)
    
    args = parser.parse_args()
    
    if torch.cuda.is_available():
        device = "cuda:"+str(args.device)
    else:
        device = "cpu"
    
    init_t, T = 0,0.5
    timegrid = np.linspace(init_t, T, args.N_steps+1)
    
    path_gen = Path_Heston(timegrid)
    model_ub = Net_SDE(1, timegrid, n_layers, vNetWidth, device)
    model_ub.to(device)
    model_lb = Net_SDE(1, timegrid, n_layers, vNetWidth, device)
    model_lb.to(device)
    
    base_lr = 0.001
    optimizer = torch.optim.Adam([model_ub.parameters()]+[model_lb.parameters()], lr=base_lr)
    
    train(model_ub, model_lb, optimizer, path_gen, args)
    
    



